## Installation

```bash
git clone https://github.com/notrab/create-react-app-redux.git
cd create-react-app-redux
yarn
```

## Get started

```bash
yarn start
```

This boilerplate is built using [create-react-app](https://github.com/facebook/create-react-app) so you will want to read the User Guide for more goodies.


## Task description

Implement a simple web app that allows one to browse the Imgur gallery using https://api.imgur.com/:

* show gallery images in a grid of thumbnails;
* show image description in the thumbnail, top or bottom;
* allow selecting the gallery section: hot, top, user;
* allow including / excluding viral images from the result set;
* allow specifying window and sort parameters;
* when clicking an image in the gallery - show its details: big image, title, description, upvotes, downvotes and score.

Bonus points:

* instead of calling Imgur API directly, proxy the API calls through your server (ex. Express);

App requirements:

* use ReactJS;
* and preferably Redux.
