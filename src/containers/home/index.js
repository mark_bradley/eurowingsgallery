import React from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchAllGalleries } from '../../actions'
import storedData from '../../mockdata/mockAPI'

const Home = props => (
  <div>
    <h1>Home</h1>

    <ul>
      {storedData.data.map(gallery => (
        <li>
          <a href={gallery.link}>{gallery.title}</a>
        </li>
      ))}
    </ul>

    <p>
      <button onClick={() => props.changePage()}>
        Go to about page via redux
      </button>
    </p>
    <p>
      <button onClick={() => props.fetchAllGalleries()}>
        Search galleries
      </button>
    </p>
  </div>
)

const mapStateToProps = ({ data = {}, isLoadingData = false }) => ({
  data,
  isLoadingData
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changePage: () => push('/about-us'),
      fetchAllGalleries: () => dispatch(fetchAllGalleries())
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  {
    fetchGalleries
  }
)(Home)
