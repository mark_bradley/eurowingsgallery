import axios from 'axios'
import { SET_GALLERIES } from './types'

const apiUrl = 'https://api.imgur.com/3/gallery/search/top/all/1?q=cats'

// export function fetchGalleries() {
//   console.log('????')
//   return apiAction({
//     url: `https://api.imgur.com/3/gallery/search/top/all/1?q=cats`,
//     onSuccess: setGalleryData,
//     onFailure: console.log('Error occured loading articles'),
//     label: FETCH_GALLERIES,
//     accessToken: '6c863e3d0a6bf8f'
//   })
// }

// //`https://api.imgur.com/3/gallery/search/${sort}/${window}/${page}?q=cats`

// function setGalleryData(data) {
//   return {
//     type: SET_GALLERIES,
//     payload: data
//   }
// }

// function apiAction({
//   url = '',
//   method = 'GET',
//   data = null,
//   accessToken = null,
//   onSuccess = () => {},
//   onFailure = () => {},
//   label = '',
//   headersOverride = null
// }) {
//   return {
//     type: API,
//     payload: {
//       url,
//       method,
//       data,
//       accessToken,
//       onSuccess,
//       onFailure,
//       label,
//       headersOverride
//     }
//   }
// }

export const setGalleries = payload => {
  return {
    type: SET_GALLERIES,
    payload
  }
}

export const fetchAllGalleries = () => {
  console.log('fetchAllGalleries')
  return (dispatch) => {
    return axios
      .get(apiUrl)
      .then(response => {
        console.log('response.data', response.data)
        dispatch(setGalleries(response.data))
      })
      .catch(error => {
        throw error
      })
  }
}
