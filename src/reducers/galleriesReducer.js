import { SET_GALLERIES } from '../actions/types'

export default function galleriesReducer(state = [], action) {
  console.log('galleries reducer')
  switch (action.type) {
    case SET_GALLERIES:
      return [...state, action.payload]
    default:
      return state
  }
}
